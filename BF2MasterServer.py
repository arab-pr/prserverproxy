import asyncio
from PlayerManager import PlayerManager
from ServersManager import ServersManager
from ServerListProxy import ServerListProxy
from AuthProxy import AuthProxy
import logging
import ipaddress

class BF2MasterServer:
    def __init__(self, bindTo="127.0.0.1", packetFilter=ipaddress.ip_network("127.0.0.1/32")):
        self.authProxy = None
        self.serverListProxy = None
        self.serversManager = None
        self.playerManager = None
        self.bindTo = bindTo
        self.packetFilter = packetFilter

    async def start(self):
        self.serversManager = ServersManager()
        self.playerManager = PlayerManager()


        logging.info("Starting BF2 master server bound to %s with subnet %s" % (self.bindTo, self.packetFilter))
        loop = asyncio.get_running_loop()
        transport, self.authProxy = await loop.create_datagram_endpoint(
            lambda: AuthProxy(playerManager=self.playerManager, serversManager=self.serversManager,
                              localIP = self.bindTo, packetFilter = self.packetFilter),
            local_addr=(self.bindTo, 29910))

        transport, self.serverListProxy = await loop.create_datagram_endpoint(
            lambda: ServerListProxy(serversManager=self.serversManager,
                                    localIP = self.bindTo, packetFilter = self.packetFilter),
            local_addr=(self.bindTo, 27900))
