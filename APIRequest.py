import aiohttp
import logging
import traceback

from aiohttp import ClientConnectionError

class APIRequest:
    def __init__(self, url, licenseKey = None, timeout=5, type="post"):
        self.url = url
        self.licenseKey = licenseKey
        self.timeout = timeout
        self.data = None
        self.type = type

    async def send(self):
        try:
            if self.licenseKey is not None:
                headers = {'PR-license-key': self.licenseKey}
            else:
                headers = None

            async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=self.timeout),
                                             headers=headers,
                                             #connector=aiohttp.TCPConnector(local_addr=None)
                                             ) as session:

                if self.type.lower() == "post":
                    response = await session.post(self.url, json=self.data)
                else:
                    response = await session.get(self.url)

                async with response:
                    if response.status != 200:
                        logging.warning(
                            "HTTP code %s, content: %s from %s" % (response.status, await response.read(), self.url))
                        return "HTTPError", response
                    return "Success", await response.read()


        except ClientConnectionError as e:
            logging.warning("Could not connect to %s: %s" % (self.url, e.strerror))
            return "Error", e
        # Unknown exception
        except Exception as e:
            exc = traceback.format_exc()
            logging.error(str(exc))
            return "Error", e
