import sys
import struct
import zlib
from io import BytesIO


# Helper function to return a null terminated string from stream
def getString(stream):
    tmp = b''
    while True:
        char = stream.read(1)
        if char == b'\0':
            return tmp
        tmp += char


# Helper function to add to struct.unpack null terminated strings
# Unpacks with null terminated strings when using "s".
# Unpacks "vehicle" structs when using "v".
# Returns a list of [value1,value2,value3...]
def unpack(stream, fmt):
    values = []

    for i in range(0, len(fmt)):
        if fmt[i] == 'v':
            vehid = unpack(stream, "h")[0]
            if vehid >= 0:
                (vehname, vehseat) = unpack(stream, "sb")
                values.append((vehid, vehname, vehseat))
            else:
                values.append( (vehid,) )

        elif fmt[i] == 's':
            string = getString(stream)
            values.append(string)
        else:
            size = struct.calcsize(fmt[i])
            values.append(struct.unpack("<" + fmt[i], stream.read(size))[0])

    return values

def pack(stream, fmt, values):
    for i in range(0, len(fmt)):
        if fmt[i] == 'v':
            veh = values[i]
            if len(veh) == 3:
                pack(stream, "hsb", veh)
            else:
                pack(stream, "h", veh)

        elif fmt[i] == 's':
            string = values[i]
            stream.write(string)
            stream.write(b"\x00")
        else:
            data = struct.pack("<" + fmt[i], values[i])
            stream.write(data)



########################################################################

# bit, struct format, whether it should be copied to minified tracker

PLAYERFLAGS = [
    (0x01, 'B', True), # team
    (0x02, 'B', True), # squad
    (0x04, 'v', True), # vehicle
    (0x08, 'b', False), # health
    (0x10, 'h', True), # score
    (0x20, 'h', False), # twscore
    (0x40, 'h', True), # kills
    (0x100, 'h', True), # deaths
    (0x0200, 'h', False), # ping
    (0x0800, 'B', True), # isalive
    (0x1000, 'B', False), # isjoining
    (0x2000, 'hhh', False), # pos
    (0x4000, 'h', False), # rot
    (0x8000, 's', True), # kit
]

VEHICLEFLAGS = [
    (0x01, 'B', True), # team
    (0x02, 'hhh', False), # position
    (0x04, 'h', False), # rot
    (0x08, 'h', False), # health
]

# how many tracker ticks between position updates
positionUpdateFrequency = 75
class MinifyTracker:
    def __init__(self, filePath, ):
        self.filepath = filePath

        try:
            compressedFile = open(filePath, 'rb')
            compressedBuffer = compressedFile.read()
        except:
            raise Exception("Error opening file")
        ####
        compressedFile.close()

        # Try to decompress or assume its not compressed if it fails
        try:
            buffer = zlib.decompress(compressedBuffer)
        except:
            buffer = compressedBuffer
        ####

        self.stream = BytesIO(buffer)
        self.length = len(buffer)
        self.output = BytesIO()
        self.playerPos = {}
        self.vehiclePos = {}

        self.tickCount = 0

        self.handlers = [self.handleDefault] * 256
        self.handlers[0x10] = self.handlePlayerUpdate
        self.handlers[0x12] = self.handlePlayerRemoved
        self.handlers[0x20] = self.handleVehicleUpdate
        self.handlers[0x22] = self.handleVehicleRemoved

    class Message:
        @classmethod
        def fromStream(cls, stream):
            lengthbytes = stream.read(2)
            if len(lengthbytes) != 2:
                return None
            # Get 2 bytes of message length
            messageLength = struct.unpack("<H", lengthbytes)[0]
            message = stream.read(messageLength)
            if len(message) != messageLength:
                return None

            return cls(message)

        def __init__(self, message):
            self.buffer = message
            self.length = len(message)
            self.stream = BytesIO(message)
            self.type = struct.unpack("<B", self.stream.read(1))[0]


    def writeMessage(self, buffer):
        if isinstance(buffer, BytesIO):
            msg = buffer.getvalue()
        else:
            msg = buffer

        pack(self.output, "H", [len(msg)])
        self.output.write(msg)

    def handlePlayerUpdate(self, message):
        outputStream = BytesIO()
        outputStream.write(b"\x10")
        while message.stream.tell() != message.length:
            outputPlayerflags = 0
            outputPlayer = BytesIO()
            flags, pid = unpack(message.stream, "HB")
            for tuple in PLAYERFLAGS:
                bit, fmt, shouldCopy = tuple
                if flags & bit:
                    # Read
                    value = unpack(message.stream, fmt)

                    # cache positions messages
                    if bit == 0x2000:
                        self.playerPos[pid] = value

                    # Write
                    if shouldCopy:
                        outputPlayerflags |= bit
                        pack(outputPlayer, fmt, value)

            if outputPlayerflags != 0:
                pack(outputStream, "HB", [outputPlayerflags, pid])
                outputStream.write(outputPlayer.getvalue())

        if len(outputStream.getvalue()) > 1:
            self.writeMessage(outputStream)

    def handlePlayerRemoved(self, message):
        while message.stream.tell() != message.length:
            pid, = unpack(message.stream, "B")
            if pid in self.playerPos:
                del self.playerPos[pid]

    def handleVehicleUpdate(self, message):
        outputStream = BytesIO()
        outputStream.write(b"\x20")
        while message.stream.tell() != message.length:
            outputVehicleflags = 0
            outputVehicle = BytesIO()
            flags, vid = unpack(message.stream, "Bh")
            for tuple in VEHICLEFLAGS:
                bit, fmt, shouldCopy = tuple
                if flags & bit:
                    # Read
                    value = unpack(message.stream, fmt)

                    # cache positions
                    if bit == 0x02:
                        self.vehiclePos[vid] = value

                    # Write
                    if shouldCopy:
                        outputVehicleflags |= bit
                        pack(outputVehicle, fmt, value)

            if outputVehicleflags != 0:
                pack(outputStream, "Bh", [outputVehicleflags, vid])
                outputStream.write(outputVehicle.getvalue())

        if len(outputStream.getvalue()) > 1:
            self.writeMessage(outputStream)


    def handleVehicleRemoved(self, message):
        while message.stream.tell() != message.length:
            vid, = unpack(message.stream, "h")
            if vid in self.vehiclePos:
                del self.vehiclePos[vid]
    # Copy message
    def handleDefault(self, message):
        pack(self.output, "H", [message.length])
        self.output.write(message.buffer)

    # Returns the message type
    def runMessage(self):
        # Check if end of file

        message = self.Message.fromStream(self.stream)
        if message is None:
            return

        self.handlers[message.type](message)

        return message.type

    def createPlayerAndVehicleUpdateMessage(self):
        out = BytesIO()
        out.write(b"\x10")
        for player in self.playerPos:
            x,y,z = self.playerPos[player]
            pack(out, "HBhhh", [0x2000, player, x,y,z])

        outputMessage = out.getvalue()
        if len(outputMessage) > 1:
            pack(self.output, "H", [len(outputMessage)])
            self.output.write(outputMessage)


        out = BytesIO()
        out.write(b"\x20")
        for vehicle in self.vehiclePos:
            x,y,z = self.vehiclePos[vehicle]
            pack(out, "Bhhhh", [0x02, vehicle, x,y,z])

        outputMessage = out.getvalue()
        if len(outputMessage) > 1:
            pack(self.output, "H", [len(outputMessage)])
            self.output.write(outputMessage)

        self.playerPos.clear()
        self.vehiclePos.clear()


    # Returns when tick or round end recieved.
    # Returns false when roundend message recieved or when unexpected EOF
    def runTick(self):
        self.tickCount += 1
        if self.tickCount % positionUpdateFrequency == 0:
            self.createPlayerAndVehicleUpdateMessage()
        while True:
            messageType = self.runMessage()
            if messageType is None:
                return False
            if messageType == 0xf1:
                return True
            if messageType == 0xf0:
                return False

    def minify(self):
        while self.runTick():
            pass

        out = zlib.compress(self.output.getvalue())
        # f = open(self.filepath + ".minified", "wb")
        # f.write(out)
        # f.close()

        return out


# if __name__ == "__main__":
#     a = MinifyTracker(r"G:\Storage\tracker_2020_07_19_17_35_19_masirah_gpm_cq_64.PRdemo")
#     a.minify()